# /usr/bin/python
#-*- coding: utf-8-*-
#
# head [-n nlin 5|10|15] [-f file]...
# 10 lineas, file o stdin
# --------------------------------
# @juan Asix M06 curs 2022-2023
# Enero 2023

# Importamos libreria
import sys, argparse
parser = argparse.ArgumentParser(description="programa que muestra n lineas de file")

# Definimos argumentos
parser.add_argument("-n", "--nlin", type=int,\
        help="Numero de lineas 5|10|15", dest="nlin",\
        metavar="[5|10|15]", default=10,\
        choices=[5,10,15])

parser.add_argument("-f", "--file", type=str,\
        help="files a procesar", metavar="file",\
        dest="file", nargs="*", action="append")

args=parser.parse_args()
print(args)

# Codigo
MAXLIN=args.nlin
fileIn=open(args.file,"r")
counter=0
for line in fileIn:
    counter+=1
    print(line, end="")
    if counter==MAXLIN: break
fileIn.close()
exit(0)
