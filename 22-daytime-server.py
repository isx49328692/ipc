# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 22-daytime-server.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001

# Creamos el socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Reutiliza el puerto
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Escucha
s.bind((HOST,PORT))
s.listen(1)

# Para cuando nos conectemos...
conn, addr = s.accept()

# Preparamos el popen
cmd = [ "date" ]
pipeData = Popen(cmd,stdout=PIPE)
for line in pipeData.stdout:
    conn.send(line)
conn.close()
sys.exit(0)
