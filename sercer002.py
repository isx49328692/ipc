# /usr/bin/python3
#-*- coding: utf-8-*-
# ps-server.py  
# -------------------------------------
# @ edt ASIX M06 Curs 22/23
# May 2023
# -------------------------------------
import sys, socket, argparse, os

# argumentos parser
parser=argparse.ArgumentParser(description="client")
parser.add_argument("-d", "--debug", action="store_true", default=False)
parser.add_argument( "-p", "--port", type=int, default=55555)
parser.add_argument("host", type=str, help="remote host")
args=parser.parse.args()

# conecta
HOST = args.host
PORT = args.port
filename = ''
payload = ''

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

# Codig
cap = s.recv(1024).decode().strip()
filename = cap[0]

fileIn = open("/tmp/" + filename, "w")

while True:
    data = s.recv(1024)
    if not data:
        break
    fileIn.write(data)
s.close()
fileIn.close()

sys.exit(0)