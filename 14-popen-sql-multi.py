# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py -d database -c numclie[...]
# --------------------------------
# @juan22 ASIX m06 curs 2022-2023
# Enero 2023

import sys, argparse
from subprocess import Popen, PIPE

listaClie=[]
parser = argparse.ArgumentParser(description = 'consulta-sql')
# sqlStatment
parser.add_argument("-d", "--database", help = "base de datos", default = "training")
parser.add_argument("-c", "--cliente", help = "cliente a consultar", \
        type = str, dest="listaClie", action="append", \
        required="True", metavar="clientes")

args = parser.parse_args()
print(args)
# ---------------------------------------------------------------------------------------------
cmd = " psql -qtA -F',' -h localhost -U postgres"+ args.database

# Creamos pipe(tunel)
pipeData = Popen(cmd, shell=True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

# Bucle que mire todos los clies que tenemos como argumentos
#for clientes in args.listaClie:


for line in pipeData.stdout:
    print(line, end="")

exit(0)

