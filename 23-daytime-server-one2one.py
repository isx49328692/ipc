# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 22-daytime-server.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, socket, argparse
from subprocess import Popen, PIPE

# Creamos argumentos
parser = argparse.ArgumentParser(description="servidor")
parser.add_argument("-p","--port", type=int, default=50001)
args=parser.parse_args()

# Creamos HOST y PORT para que se les pueda asignar un valor
HOST = ''
PORT = args.port

# Creamos el socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Reutiliza el puerto
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Escucha
s.bind((HOST,PORT))
s.listen(1)

# Creamos bucle para que al finalizar una conexion, no cierre todo
while True:
    # Para cuando nos conectemos...
    conn, addr = s.accept()

    # Preparamos el popen
    cmd = [ "date" ]
    pipeData = Popen(cmd,stdout=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()
#    sys.exit(0)
