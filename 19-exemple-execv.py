# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-execv.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------

import sys, os

print("hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    print("Programa pare", os.getpid(), pid)
    sys.exit(0)

print("Programa fill", os.getpid(), pid)

#os.execv("/usr/bin/ls",["/usr/bin/ls","-la","/"])

# -----ejemplo execv--------
os.execl()

# ---------------Esta parte del codigo no se ejecutara nunca
print("Hasta luego lucas!")
sys.exit(0)
