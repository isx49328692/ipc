# /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal-exemple.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------

import sys, os, signal

# -------Definimos handlers----------------
def myusr1(signum, frame):
    print("Signal handler with signal:", signum)
    print("hola radiola")

def myusr2(signum, frame):
    print("Signal handler with signal:", signum)
    print("adeu andrew")
    sys.exit(0)
# --------------

# ---------Codigo-----------------
print("hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    print("Programa pare", os.getpid(), pid)
    print("hasta luego lucas")
    sys.exit(0)


# ------------Codigo programa hijo ---------------------
print("Programa fill", os.getpid(), pid)

signal.signal(signal.SIGUSR1, myusr1)
signal.signal(signal.SIGUSR2, myusr2)
while True:
    pass
print("Hasta luego lucas")
sys.exit(0)
