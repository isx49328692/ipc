# /usr/bin/python3
#-*- coding: utf-8-*-
# ps-server.py  
# -------------------------------------
# @ edt ASIX M06 Curs 22/23
# May 2023
# -------------------------------------
import sys, socket, argparse, os, signal, datetime
from subprocess import Popen, PIPE

# Defino argumentos
parser=argparse.ArgumentParser(description="server")
parser.add_argument( "-d", "--debug", action="store_true", default=False)
parser.add_argument( "-p", "--port", type=int, default=55555)
args=parser.parse_args()

#----
HOST = ''
PORT = args.port
listaPeers = []
trans = {}
MYEOF = bytes(chr(4),'utf-8')

# defino handlers
def mysigusr1(signum, frame):
    print("Signal handler called with signal:", signum)
    print(listaPeers)
    sys.exit(0)

def mysigusr2(signum, frame):
    print("Signal handler called with signal:", signum)
    print(len(listaPeers))
    sys.exit(0)

def mysigterm(signum, frame):
    print("Signal handler called with signal:", signum)
    print(len(listaPeers), (listaPeers))
    sys.exit(0)

for peer in listaPeers:
    ip = peer[0]
    print(ip)

# Creamos Daemon
pid=os.fork()
if pid != 0:
    print("Server encendido", pid)
    sys.exit(0)

# asignamos signals
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)

# Creamos socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
## Reutilizar puerto
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Vinculamos HOST y PORT; "Escuchamos"
s.bind((HOST,PORT))
s.listen(1)

# Codigo
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    listaPeers.append(addr)

    # Creamos ip, fecha
    ip = addr[0]
    fecha = datetime.datetime.now().strftime("%Y%m%d-%H%M%S%f")

    # version
    if ip in trans:
        trans[ip] += 1
    else:
        trans[ip] = 1
    version = trans[ip]
    
    # juntamos nombre file
    filename = f"/tmp/{ip}-{fecha}-{version}"
    #filename = "/tmp/{}-{}-{}".format(ip, now, version)

    # Creamos y escribimos
    fileIn=open(filename, "w")
    while True:
        data = conn.recv(1024)
        if not data: break
        fileIn.write(str(data))
    conn.close
    fileIn.close()
s.close(0)
sys.exit(0)