# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py ruta
# 
# --------------------------------
# Juan22 ASIX M06 curs 2022-2023
# Enero 2023
# --------------------------------

import sys, argparse

from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """Exemple popen""")
parser.add_argument("ruta", type=str,\
        help="directori a llistar")
args = parser.parse_args()
#print(args)
# -------------------------------------------
command = ["ls", "/opt",args.ruta]
#command = [ "who" ]

# Creamos pipe(tunel)
pipeData = Popen(command, stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")

exit(0)

