# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 26-telnet-server-one2one.py
# -------------------------------------
# @juan22
# Enero 2023
# -------------------------------------
import sys, socket, argparse, os, signal
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="server")
parser.add_argument("-p","--port", type=int, default=50001)
parser.add_argument("-d", "--debug", action="store_true", help="trace de les accions")

args=parser.parse_args()

# Creamos HOST y PORT para asignarle un valor
HOST = ''
PORT = args.port
YATA = bytes(chr(4), 'utf-8')
DEBUG = args.debug

## Creamos socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
## Reutiliza el puerto
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

## bind --> vincula el host y el puerto / indica por cual puerto debe escuchar (enps0, loopback, docker0, docker1) --> HOST --> cual interficie
s.bind((HOST,PORT))
## Escucha
s.listen(1)

# codigo
while True:
    conn, addr = s.accept()
    print("connected by", addr)

    while True:
        cmd = conn.recv(1024)
        if DEBUG: print('Recive', repr(cmd))
        if not cmd: break
        pipeData = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        for line in pipeData.stdout:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line), 'utf-8')
        for line in pipeData.stderr:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line), 'utf-8')
        conn.sendall(YATA)
    conn.close()
s.close()
sys.exit(0)
