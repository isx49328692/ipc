# /usr/bin/python
#-*- coding: utf-8-*-
#
# head [file]
# 10 lineas, file o stdin
# --------------------------------
# @juan Asix M06 curs 2022-2023
# Enero 2023

# Importamos libreria
import argparse

# argparse --> modul (dentro del modulo argparse llama a la funcion [ArgumentParser] --> constructor (objecte argparse)
parser = argparse.ArgumentParser(\
        description="programa exemple arguments",\
        prog="02-arguments.py",\
        epilog="hasta luego Lucas!")
# Añadimos argumentos
parser.add_argument("-e","--edat", type=int, dest="useredat", help="edat a processar",\
        metavar="edat")
parser.add_argument("-n", "--nom", type=str,\
        help="nom de usuari")
args=parser.parse_args()
print(args)
print(args.useredat, args.nom)
