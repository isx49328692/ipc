#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# server01.py [-d --debug] [-p --port]
#
#--------------------------------------------------------------
import sys, signal, argparse, os, socket
from subprocess import Popen, PIPE

# --- Creamos "args"-------------------------------------------
parser = argparse.ArgumentParser(description="Server01")
parser.add_argument("-d","--debug", action="store_true")
parser.add_argument("-p","--port", type=int, default=44444)
args=parser.parse_args()

# ---
HOST = ''
PORT = args.port
listaCon=[]
DEBUG = args.debug

# ----Definimos handlers ----------------------------------------
def mysigusr1(signum, frame):
    print("signal handler called with signal:", signum)
    print(listaCon)
    sys.exit(0)
def mysigusr2(signum, frame):
    print("signal handler called with signal:", signum)
    print(len(listaCon))
    sys.exit(0)
def mysigterm(signum, frame):
    print("signal handler called with signal:", signum)
    print(listaCon, len(listaCon))
    sys.exit(0)

# ------Fork----------------------------------------------------------
pid=os.fork()
if pid != 0:
    print("programa pare", pid)
    sys.exit(0)

# -----Asignamos signals -----------------------------------------
signal.signal(signal.SIGUSR1, mysigusr1)
signal.signal(signal.SIGUSR2, mysigusr2)
signal.signal(signal.SIGTERM, mysigterm)

# Creamos socket----------------------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
## reutilizar puerto
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# vinculamos HOST y PORT; "escuchamos" ------------
s.bind((HOST,PORT))
s.listen(1)

# Codigo -----------------------------------------------------------
while True:
    conn, addr = s.accept()
    listaCon.append(addr)
    print("connected by", addr)
    while True:
        cmd = conn.recv(1024)
#        print("0",cmd)
        if DEBUG: print("recive", repr(cmd))
        if cmd == "processos":
            cmd = "ps ax"
#            print("1",cmd)
        elif cmd == "ports":
            cmd = "netstat -puta"
#            print("2",cmd)
        elif cmd == "whoareyou":
            cmd = "uname -a"
#            print("3",cmd)
        else:
            cmd = "uname -a"
#            print("4",cmd)
        pipeData = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        for line in pipeData.stdout:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line), 'utf-8')
        for line in pipeData.stderr:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line), 'utf-8')
    conn.close()
s.close()
sys.exit(0)