# /usr/bin/python3
#-*- coding: utf-8-*-
#
# Juan Sanchez asix m06 22-23
# May 24
# --------------------------------

import sys, socket, argparse, os, signal, time
from subprocess import Popen, PIPE

# defino argumentos parser
parser=argparse.ArgumentParser(description="server")
parser.add_argument( "-d", "--debug", action="store_true", default=False)
parser.add_argument( "-p", "--port", type=int, default=50001)
args=parser.parse_args()

#---
HOST = ''
PORT = args.port
listaPeers = []
MYEOF = bytes(chr(4),'utf-8')
connections = []

# definir handlers
def mysigusr1(signum, frame):
    print("Signal handler called with signal:", signum)
    for conn in connections:
        timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(conn['timestamp']))
        print(f" {listaPeers} ({timestamp})")
    sys.exit(0)

def mysigusr2(signum, frame):
    print("Signal handler called with signal:", signum)
    print(list(set(map(tuple, listaPeers))))
    sys.exit(0)

def mysighup(signum, frame):
    print("signal handler called with signal:", signum)
    global connections
    connections = []
    print("Registros de conexion iniciado")
    sys.exit(0)

def mysigterm(signum, frame):
    print("Signal handler called with signal:", signum)
    print(len(listaPeers), listaPeers)
    sys.exit(0)

# daemon
pid=os.fork()
if pid != 0:
    print("Server encendido", pid)
    sys.exit(0)

# signals
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGHUP,mysighup)
signal.signal(signal.SIGTERM,mysigterm)

# Creamos socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
## Reutilizar puerto
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Vinculamos HOST y PORT; "Escuchamos"
s.bind((HOST,PORT))
s.listen(1)

# Codigo
while True:
    conn, addr =s.accept()
    print("Connected by", addr)
    while True:
        # bucle para leer numero hasta recibir señal de fi
        data = b''
        while True:
            data2 = conn.recv(1024)
            if not data2:
                break
            if data2[:-1] == MYEOF:
                break
        if args.debug:
            print("data: %s" %(data))
        if not data:
            if args.debug:
                print ("La conexion con %s ha cerrado" %(addr[0]))
            conn.close()
            break
        if args.debug:
            print("Pregunta por el numero: %s" %(data))
        cmd = 'openssl prime '  + data.decode("utf-8")
        pipeData = Popen(cmd,shell=True,stdout=PIPE)
        if not data: break
        for line in pipeData.stdout:
            conn.send(line)
            if args.debug:
                print ("enviando: %s" %(line))
        conn.send(MYEOF)
    conn.close(0)
s.close(0)
sys.exit(0)