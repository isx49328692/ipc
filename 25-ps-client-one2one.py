# /usr/bin/python3
#-*- coding: utf-8-*-
# 25-ps-client-one2one.py  
# -------------------------------------
# @juan22
# Enero 2023
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE

# Creamos los argumentos
parser = argparse.ArgumentParser(description="client")
parser.add_argument("server", type=str)
parser.add_argument("-p","--port", type=int, default=50001)

args=parser.parse_args()

# Creamos HOST y PORT
HOST = args.server
PORT = args.port

# Creamos socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

# Asignamos el comando
cmd = "ps ax"

pipeData = Popen(cmd, shell=True, stdout=PIPE)
for line in pipeData.stdout:
    # Mandamos la salida al servidor
    s.send(line)

# Cerramos
s.close()
sys.exit(0)
