# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py consulta-sql
# --------------------------------
# @juan22 ASIX m06 curs 2022-2023
# Enero 2023

import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description = 'consulta-sql')
# sqlStatment
parser.add_argument("sqlStatment", \
        help = "consulta sql", \
        metavar = "consulta")

args = parser.parse_args()
# ---------------------------------------------------------------------------------------------
cmd = " psql -qtA -F',' -h localhost -U postgres training"

# Creamos pipe(tunel)
pipeData = Popen(cmd,shell=True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
#pipeData.stdin.write("select * from oficnas;\n\q\n")
pipeData.stdin.write(args.sqlStatment+"\n\q\n")
for line in pipeData.stdout:
    print(line, end="")

exit(0)
