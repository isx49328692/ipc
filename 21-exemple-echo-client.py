# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, socket

HOST = ''
#HOST = 'g18'
PORT = 50001
#PORT = 7

#socket.socket crea un objeto, AF_INET --> crea un socket estandar de internet / SOCKET_STREAM --> indica que nos cree un socket UDP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))
s.send(b'Hello, world')
# recv --> escucha / 1024 --> sin importancia por el momento
data = s.recv(1024)
s.close()
print('Recived', repr(data))
sys.exit(0)
