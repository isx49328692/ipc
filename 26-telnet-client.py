# /usr/bin/python3
#-*- coding: utf-8-*-
# 26-telnet-client.py -port -s server
# -------------------------------------
# @juan22
# Enero 2023
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE

# Creamos los argumentos
parser = argparse.ArgumentParser(description="client")
parser.add_argument("-s","--server", type=str)
parser.add_argument("-p","--port", type=int, default=50001)

args=parser.parse_args()

# Creamos HOST y PORT
HOST = args.server
PORT = args.port
YATA = bytes(chr(4), 'utf-8')

# Creamos socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))

while True:
    cmd = input("~$ ")
    if not cmd: break
    # if yata: break
    # sendall --> Hace un "flush" vacia todo
    s.sendall(bytes(cmd, 'utf-8'))

    while True:
        data = s.recv(1024)
        # Si la ultima parte de data (como es una cadena seria [-1:]) es 4, cierra
        if data[-1:] == YATA:
            print(str(data[:-1]))
            break
        print(str(data))
s.close()
sys.exit(0)
