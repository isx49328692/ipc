# /usr/bin/python3
#-*- coding: utf-8-*-
#
# Es como el programa anterior, pero,
# ejecuta el programa 16-signal.py amb 70 segons
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, os

# ----------  Programa anterior ------------------
print("hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    print("Programa pare", os.getpid(), pid)
    sys.exit(0)

print("Programa fill", os.getpid(), pid)

# ---------- Nuevo codigo con exec -------------------
os.execv("/usr/bin/python3",["/usr/bin/python3","16-signal.py","60"])

print("Hasta luego licas")
sys.exit(0)

