# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 22-daytime-client.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------------------------------------------
# Para probar que funciona el cliente:
#   nc -l 50001
# Encendemos el cliente
# Tambien podemos hacer --> telnet localhost 13 / encendemos el cliente
# --------------------------------------------------------------------------
import sys, socket, argparse

# Creamos los argumentos
parser = argparse.ArgumentParser(description="cliente")
parser.add_argument("-s","--server", type=str, default='localhost')
parser.add_argument("-p","--port", type=int, default=50001)
args=parser.parse_args()

# asignamos argumentos a HOST y PORT para el socket
HOST = args.server
PORT = args.port

# Creamos socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

# Escuchamos al servidor
while True:
    data = s.recv(1024)
    # if not data --> si el otro extremo a cerrado la connexion
    if not data: break
    # Mostramos lo recibido
    print(repr(data))

# Mostramos lo recibido
# print(repr(data))
s.close()
sys.exit(0)
