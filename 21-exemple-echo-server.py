# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, socket

HOST = ''
PORT = 50001

#socket.socket crea un objeto, AF_INET --> crea un socket estandar de internet / SOCKET_STREAM --> indica que nos cree un socket UDP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# bind escucha¿?
s.bind((HOST,PORT))
# que se quede escuchando / 1 --> sin importancia
s.listen(1)
# cuando alguien se connecta se sigue a la siguiente linea
# conn --> objeto que representa la conexion
conn, addr = s.accept()
print("Conn", type(conn), conn)
print("Connected by;", addr)
while True:
    data = conn.recv(1024)
    if not data: break
    conn.send(data)
conn.close()
sys.exit(0)
