# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py
# --------------------------------
# @juan22 ASIX m06 curs 2022-2023
# Enero 2023

import sys, argparse
from subprocess import Popen, PIPE

command = " psql -qtA -F',' -h localhost -U postgres training -c \"select * from clientes;  \""

# Creamos pipe(tunel)
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")

exit(0)
