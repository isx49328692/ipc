# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 22-calendar-server-one2one.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, socket, argparse, os, signal
from subprocess import Popen, PIPE

# Creamos argumentos
parser = argparse.ArgumentParser(description="server")
parser.add_argument("-p","--port", type=int, default=50001)
parser.add_argument("-a","--any", type=int, default=2023)

args=parser.parse_args()

# Creamos la lista donde guardar las conexiones que vaya a tener con un append
listaCon=[]

# Creamos HOST, PORT y ANY para que se les pueda asignar un valor
HOST = ''
PORT = args.port
ANY = args.any

# -------- Definimos headler --------------------------------
def mysigusr1(signum, frame):
    print("signal handler with signal:", signum)
    print(listaCon)
    sys.exit(0)

def mysigusr2(signum, frame):
    print("signal handler with signal:", signum)
    print(len(listaCon))
    sys.exit(0)

def mysigterm(signum, frame):
    print("signal handler with signal:", signum)
    print(listaCon, len(listaCon))
    sys.exit(0)

# Codigo ---- Fork---------------------------------
pid=os.fork()
if pid != 0:
    print("Programa pare", pid)
    sys.exit(0)

# Codigo programa hijo y señales -------------------
signal.signal(signal.SIGUSR1, mysigusr1)
signal.signal(signal.SIGUSR2, mysigusr2)
signal.signal(signal.SIGTERM, mysigterm)

## Creamos socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
## Reutiliza el puerto
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

## bind --> vincula el host y el puerto / indica por cual puerto debe escuchar (enps0, loopback, docker0, docker1) --> HOST --> cual interficie
s.bind((HOST,PORT))
## Escucha
s.listen(1)

## codigo
while True:
    conn, addr = s.accept()
    # Añadimos la conexion a la listaCon
    listaCon.append(addr)
    # asignamos valor al comando .. cmd
    cmd = "cal %d" % (ANY)
    pipeData = Popen(cmd, shell=True, stdout=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()
