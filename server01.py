# /usr/bin/python3
#-*- coding: utf-8-*-
# ps-server.py  
# -------------------------------------
# @ edt ASIX M06 Curs 22/23
# May 2023
# -------------------------------------

import sys, socket, argparse, os, signal
from subprocess import Popen, PIPE

# defino argumentos parser
parser=argparse.ArgumentParser(description="server")
parser.add_argument( "-d", "--debug", action="store_true", default=False)
parser.add_argument( "-p", "--port", type=int, default=44444)
args=parser.parse_args()

#---
HOST = ''
PORT = args.port
listaPeers = []
MYEOF = bytes(chr(4),'utf-8')

# definir handlers
def mysigusr1(signum, frame):
    print("Signal handler called with signal:", signum)
    print(listaPeers)
    sys.exit(0)

def mysigusr2(signum, frame):
    print("Signal handler called with signal:", signum)
    print(len(listaPeers))
    sys.exit(0)

def mysigterm(signum, frame):
    print("Signal handler called with signal:", signum)
    print(len(listaPeers), listaPeers)
    sys.exit(0)

#--------------------------------
pid=os.fork()
if pid != 0:
    print("Server encendido", pid)
    sys.exit(0)

# asignamos signals
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)

# Creamos socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
## Reutilizar puerto
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Vinculamos HOST y PORT; "Escuchamos"
s.bind((HOST,PORT))
s.listen(1)

# Codigo
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    listaPeers.append(addr)

    while True:
        cmd = conn.recv(1024)
        if args.debug:
            print ("cmd: %s" %(cmd))
        if not cmd:
            if args.debug:
                print ("La conexion con %s ha cerrado" % (addr[0]))
            conn.close()
            break
        if args.debug:
            print("Comando recibido: %s" %(cmd))
        if cmd.decode("utf-8")[:-1] == "processos":
            cmd = "ps ax"
        elif cmd.decode("utf-8")[:-1] == "ports":
            cmd = "netstar -puta"
        else:
            cmd = "uname -a"
        
        pipeData = Popen(cmd, shell=True,stdout=PIPE)

        for line in pipeData.stdout:
            conn.sendall(line)

            if args.debug:
                print ("Enviando: %s" %(line))
            
        conn.send(MYEOF)
    conn.close()

s.close(0)
sys.exit(0)